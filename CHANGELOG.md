## Changelog livebuild_dflinuxe12 - Bookworm ##

###### 12/01/2025 - VERSION 12.9
 * Modification DFbuild.sh VERSION="12.9"
 * [Nouvelle version stable Debian 12 Bookworm](https://www.debian.org/News/2025/20250111 "Publication de la neuvième mise à jour de la distribution stable Debian 12")
- Mise à jour du fichier /doc/dflinuxe12_desktop.pdf et .ods
- Modification du fichier /Documents/dflinuxe12_desktop.pdf
- Correction du lien "le guide live build" du fichier README.md

###### 10/11/2024 - VERSION 12.8
 * Modification DFbuild.sh VERSION="12.8"
 * [Nouvelle version stable Debian 12 Bookworm](https://www.debian.org/News/2024/20241109 "Publication de la huitième mise à jour de la distribution stable Debian 12")
- Mise à jour du fichier /doc/dflinuxe12_desktop.pdf et .ods
- Modification du fichier /Documents/dflinuxe12_desktop.pdf
- Correction des DNS sur fichier /system-connections/Connexion filaire 1. Remplacement des [DNS de FDN](https://www.fdn.fr/actions/dns/ "Résolveurs DNS ouverts FDN") par [les DNS de DNS.SB](https://dns.sb/ "DNS.SB believes, without privacy there is no human dignity.")  Information de [Stéphane Bortzmeyer](https://www.bortzmeyer.org/dns-sb.html "Le résolveur DNS public dns.sb")

###### 23/09/2024 - VERSION 12.7
 * Modification DFbuild.sh VERSION="12.7"
 * [Nouvelle version stable Debian 12 Bookworm](https://www.debian.org/News/2024/20240831 "Publication de la septième mise à jour de la distribution stable Debian 12")
- Mise à jour du fichier /doc/dflinuxe12_desktop.pdf et .ods
- Modification du fichier /Documents/dflinuxe12_desktop.pdf
- Passage du md5sum au sha256sum pour la vérification de l'ISO
- Donner la priorité aux paquets debian (.deb) sur les flatpacks dans "Logiciels"

###### 29/06/2024 - VERSION 12.6
 * Modification DFbuild.sh VERSION="12.6"
 * [Nouvelle version stable Debian 12.6 Bookworm](https://www.debian.org/News/2024/20240629 "Publication de la sixième mise à jour de la distribution stable Debian 12")
 * Mise à jour du fichier /doc/dflinuxe12_desktop.pdf et .ods
 * Modification du fichier /Documents/dflinuxe12_desktop.pdf
 
 
###### 11/02/2024 - VERSION 12.5
 * Modification DFbuild.sh VERSION="12.5"
 * [Nouvelle version stable Debian 12.5 Bookworm](https://www.debian.org/News/2024/20240210 "Publication de la cinquième mise à jour de la distribution stable Debian 12")
 * Mise à jour du fichier /doc/dflinuxe12_desktop.pdf et .ods
 * Modification du fichier /Documents/dflinuxe12_desktop.pdf

###### 11/12/2023 - VERSION 12.4
 * Modification DFbuild.sh VERSION="12.4"
 * [Nouvelle version stable Debian 12.4 Bookworm](https://www.debian.org/News/2023/20231210 "Publication de la quatrième mise à jour de la distribution stable Debian 12")
 * Cette version ponctuelle ajoute principalement des corrections pour des problèmes de sécurité, ainsi que quelques ajustements. L'équipe de publication a été informée en cours de cycle 12.3 qu'il y avait une possibilité de corruption du système de fichiers avec linux-image-6.1.0-14. Debian 12.4 remplace Debian 12.3.
 * Mise à jour du fichier /doc/dflinuxe12_desktop.pdf et .ods
 * Modification fichier /Documents/dflinuxe12_desktop.pdf
 * Correction du fichier /config/hooks/normal/user_avatar.chroot
 
###### 10/12/2023 - VERSION 12.3
 * [Debian 12.4 remplace Debian 12.3 Bookworm](https://www.debian.org/News/2023/2023121002 "L’équipe de publication a été informée en partie au cours du cycle de publication qu’il y avait un risque de corruption du système de fichiers avec linux-image-6.1.0-14*")
 
###### 08/10/2023 - VERSION 12.2
 * Modification DFbuild.sh VERSION="12.2"
 * [Nouvelle version stable Debian 12 Bookworm](https://www.debian.org/News/2023/20231007 "07/10/23 Publication de la deuxième mise à jour de la distribution stable Debian 12.2")
 * Modification fichiers /doc/dflinuxe12_desktop.pdf et .ods
 * Mise à jour du fichier /Documents/dflinuxe12_desktop.pdf
 
###### 23/07/2023 - VERSION 12.1
 * Modification DFbuild.sh VERSION="12.1"
 * [Version stable Debian 12.1 Bookworm](https://www.debian.org/News/2023/20230722 "22/07/2023 Publication de la première mise à jour de la distribution stable Debian 12.1")
 * Modification fichiers /doc/dflinuxe12_desktop.pdf et .ods
 * Mise à jour du fichier /Documents/dflinuxe12_desktop.pdf
 * Ajout du fichier thunar12.pdf mode d"emploi de Thunar
 * Modification fichier /doc/dflinuxe12_arborescence.txt


###### 19/07/2023 - VERSION 12.0b
 * Modification DFbuild.sh VERSION="12.0b"
 * Modification fichiers /doc/dflinuxe12_desktop.pdf et .ods
 * Mise à jour du fichier /Documents/dflinuxe12_desktop.pdf
 * Mise à jour du fichier /config/package-lists/dflinuxe.list.chroot
 * Modification des fichiers /.mozilla/firefox/dflinuxe.default
 * Ajout du fichier config/hooks/normal/firmware-clean.chroot
 * Modification fichier /doc/dflinuxe12_arborescence.txt

###### 03/07/2023 - VERSION 12.0a
 * Modification DFbuild.sh VERSION="12.0a"
 * Modification fichiers /doc/dflinuxe12_desktop.pdf et .ods
 * Mise à jour du fichier /Documents/dflinuxe12_desktop.pdf
 * Modification /install/df-preseed.cfg et ajout du fichier /usr/share/dflinuxe/dfpreseed pour supprimer les paquets live-* après installation
 * Mise à jour du fichier /doc/dflinuxe12_arborescence.txt
 * Modification du fichier /usr/share/dfiso/late-setup.sh pour suppression Debian12-login-live.png après installation

###### 27/06/2023 - VERSION 12.0
 * Modification DFbuild.sh VERSION="12.0"
 * [Nouvelle version stable Debian 12 Bookworm](https://www.debian.org/News/2023/20230610 "10/06/23 Après un an, neuf mois et vingt-huit jours de développement, le projet Debian est fier d'annoncer sa nouvelle version stable n°12 : nom de code Bookworm")
 * Mise à jour du fichier /Documents/tuto_miseajour.pdf
 * Modification fichiers /doc/dflinuxe12_desktop.pdf et .ods
 * Mise à jour du fichier /Documents/dflinuxe12_desktop.pdf
 * Nettoyage des fonds d'écran (- 12)
 * Suppression des dépendances dans dflinuxe.list.chroot
 * Suppression de l'extension HTTPS Everywhere

###### 30/05/2023 - VERSION 12.rc4
 * Modification DFbuild.sh VERSION="12.rc4"
 * [base installateur Debian Bookworm RC 4](https://www.debian.org/devel/debian-installer/News/2023/20230527 "27/05/23 Publication de l'installateur Debian Bookworm RC 4")
 * Typo date 16/05/23 Publication de l'installateur Debian Bookworm RC 3
 * Mise à jour du fichier Documents/dflinuxe12_desktop.pdf
 * Modification fichiers /doc/dflinuxe12_desktop.pdf et .ods
 * Modifications fichiers de Firefox : xulstore.json et prefs.js

###### 20/05/2023 - VERSION 12.rc3
 * Modification DFbuild.sh VERSION="12.rc3"
 * [base installateur Debian Bookworm RC 3](https://www.debian.org/devel/debian-installer/News/2023/20230516 "16/05/23 Publication de l'installateur Debian Bookworm RC 3")
 * Mise à jour du fichier /Documents/tuto_miseajour.pdf
 * Modification fichiers  /doc/dflinuxe12_desktop.pdf et .ods
 * Mise à jour du fichier /Documents/dflinuxe12_desktop.pdf

###### 12/05/2023 - VERSION 12.rc2
 * Modification DFbuild.sh VERSION="12.rc2"
 * [base installateur Debian Bookworm RC 2](https://www.debian.org/devel/debian-installer/News/2023/20230428 "03/04/23 Publication de l'installateur Debian Bookworm RC 2")
 * Modification property name="autophoto" du fichier thunar-volman.xml
 * Ajout property name="misc-volume-management" true et name="misc-text-beside-icons" false du fichier thunar.xml
 * Mise à jour du fichier /Documents/tuto_miseajour.pdf

###### 15/03/2023 - VERSION 12.rc1a
 * Modification DFbuild.sh VERSION="12.rc1a"
 * Modification fichier bookworm96.png
 * Modification fichier Debian12-login-live.png
 * Modification fichier lightdm-gtk-greeter.conf avec bookworm96.png
 * Modification fichier /dflinuxe/bienvenue.sh à définir comme approuvé
 * Correction typo fichier ristretto.xml
 * Suppresion de l'installateur Calamares


###### 04/03/2023 - VERSION 12.rc1
 * Modification DFbuild.sh VERSION="12.rc1"
 * [base installateur Debian Bookworm RC 1](https://www.debian.org/devel/debian-installer/News/2023/20230403 "03/04/23 Publication de l'installateur Debian Bookworm RC 1")
 * Modification fichiers dflinuxe12_desktop.pdf et .ods
 * Ajout fichier /Documents/dflinuxe12_desktop.pdf
 * Modification fichier /auto/config
 * Correction fichier config/hooks/normal/user_avatar.chroot	


###### 28/03/2023 - VERSION 12.a2
 * [base installateur Debian Bookworm Alpha 2](https://www.debian.org/devel/debian-installer/News/2023/20230219 "19/02/23 Publication de l'installateur Debian Bookworm Alpha 2")
 * Modification du README.md
 * Initialisation des fichiers / + /doc + /auto /config 
 * Création du CHANGELOG.md , TODO.md, CONTRIBUTING.md

###### 27/03/2023
 * Initialisation du dossier livebuild_dflinuxe12
 * Ajout README


###### Contact
  * [Me contacter](http://jc.etiemble.free.fr/ "Site Web perso")
