#!/bin/bash

#Mettre à jour les mirroirs Debian
cp "/usr/share/dflinuxe/sources.list" "/etc/apt/sources.list"

#fix nm profile
rm -f "/etc/NetworkManager/system-connections/Wired connection 1"

#Changer le fond de lightdm en mode live  ## 24/03/23 + sinon nettoyer 03/07/23
if [ $(df '/' --output=source | tail -n1) == 'overlay' ]; then
		mv "/usr/share/backgrounds/xfce/Debian12-login-live.png" "/usr/share/backgrounds/xfce/Debian12-login.png"
	else
		rm "/usr/share/backgrounds/xfce/Debian12-login-live.png"
fi

#Configuration de grub
sed -ri 's|^(GRUB_CMDLINE_LINUX_DEFAULT=)"([^"]*)"|\1"\2 splash"|' "/etc/default/grub"
sed -ri 's|^(GRUB_CMDLINE_LINUX=).*|\1"acpi_osi=Linux"|' "/etc/default/grub"
update-grub


#run once ; then autodestory !
systemctl disable late-setup
rm "/etc/systemd/system/late-setup.service"
systemctl daemon-reload
systemctl reset-failed
